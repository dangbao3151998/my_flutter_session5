import 'package:flutter/material.dart';
import "package:session4/components/splash.dart";
import 'package:session4/routes/path.dart';


class SplashScreen extends StatefulWidget {
  static String path = splashPath;

  const SplashScreen({ Key? key }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(initialPage: 0,);

    return  Scaffold(
      body: Splash(),
    );
  }
}
