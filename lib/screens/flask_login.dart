import 'package:flutter/material.dart';
import 'package:session4/routes/path.dart';

class FlaskLogin extends StatefulWidget {
  static String path = flaskLogin;
  const FlaskLogin({ Key? key }) : super(key: key);

  @override
  _FlaskLoginScreenState createState() => _FlaskLoginScreenState();
}

class _FlaskLoginScreenState extends State<FlaskLogin> {
  final _formKey = GlobalKey<FormState>();
  bool isObscureText = true;

  var _isLoading = false;
  void _onSubmit() {
    setState(() => _isLoading = true);
    Navigator.pushNamed(context, loginPath);
    Future.delayed(
      const Duration(seconds: 2),

          () => setState(() => {_isLoading = false}),
    );

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFf6f9ff),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Container(
                  child: Center(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20), child:
                  Container(
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const SizedBox(height: 50,),
                          Image.asset('assets/images/image3.png'),
                          const SizedBox(height: 50,),
                          const Text('GPS Tracking', style: TextStyle(color: Color(0xFF353536), fontSize: 24, fontWeight: FontWeight.w600),),
                          const SizedBox(height: 20,),
                          const Text('Loved tje class! Such beautiful land and collective impact infrastructure social \n entrepreneur.', style: TextStyle(color: Color(0xFF69798D), fontSize: 16, fontWeight: FontWeight.w400),textAlign: TextAlign.center,),
                          const SizedBox(height: 50,),
                          Container(
                            child: Stack(
                                children: <Widget>[
                                  Container(
                                    // duration: kAnimationDuration,
                                    margin: EdgeInsets.only(left: 40),
                                    height: 7,
                                    width:  20,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                  ),
                                  Container(
                                    // duration: kAnimationDuration,
                                    margin: EdgeInsets.only(left:24),
                                    height: 7,
                                    width:  6,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD8D8D8),
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                  ),
                                  Container(
                                    // duration: kAnimationDuration,
                                    margin: EdgeInsets.only(left: 32),
                                    height: 7,
                                    width:  6,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD8D8D8),
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                  ),
                                ]
                            )
                          ),
                          Form(
                              key: _formKey,
                              child: Column(children: [
                                const SizedBox(height: 20,),
                                Container(
                                    height: 55,
                                    width: 350,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
                                        color: const Color(0xFF4167AF)
                                    ),
                                    child: TextButton(
                                      onPressed: _isLoading ? null :  _onSubmit,
                                      child: Container(
                                        padding: const EdgeInsets.all(10),
                                        child: _isLoading
                                            ? const SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CircularProgressIndicator(),
                                        )
                                            : const Text('SING IN WITH FACEBOOK', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),),
                                      ),
                                    )
                                ),
                                const SizedBox(height: 20,),
                                Container(
                                    height: 55,
                                    width: 350,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
                                        color: const Color(0xFFFA8266)
                                    ),
                                    child: TextButton(
                                      onPressed: _isLoading ? null :  _onSubmit,
                                      child: Container(
                                        padding: const EdgeInsets.all(10),
                                        child: _isLoading
                                            ? const SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CircularProgressIndicator(),
                                        )
                                            : const Text('SING IN', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),),
                                      ),
                                    )
                                ),
                                const SizedBox(height: 20,),
                                Container(
                                    child: Center(child:
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const Text('Or Start to', style: TextStyle(color: Color(0xFF69798D), fontWeight: FontWeight.w400, fontSize: 14),),
                                        const SizedBox(width: 10,),
                                        GestureDetector(onTap: (){
                                          Navigator.pushNamed(context, registerPath);
                                        },
                                            child: const Text('Search Now', style: TextStyle(color: Color(0xFFFA8266), fontSize: 18, fontWeight: FontWeight.w600),)
                                        )
                                      ],)
                                    )
                                ),
                                const SizedBox(height: 20,),
                              ],))
                        ],))
                  )
                  )
              )
          )
      ),
    );
  }
}