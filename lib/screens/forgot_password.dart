import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:session4/routes/path.dart';
// import 'package:flutter_switch/flutter_switch.dart';

class ForgotPasswordScreen extends StatefulWidget {
  static String path = forgotPasswordPath;
  const ForgotPasswordScreen({ Key? key }) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  bool isObscureText = true;
  bool status = false;
  var _isLoading = false;
  String email ='';

  void _onSubmit() {
    setState(() => _isLoading = true);
    Future.delayed(
      const Duration(seconds: 2),
          () => setState(() => {_isLoading = false}),
    );

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFf6f9ff),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Container(
                  child: Center(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20), child:
                  Container(
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 50,),
                          const Text('FORGOT PASSWORD', style: TextStyle(color: Color(0xFF404040), fontSize: 24, fontWeight: FontWeight.w600),),
                          const SizedBox(height: 20,),
                          const Text('Enter your mail or phone to request a password reset', style: TextStyle(color: Color(0xFF69798D), fontSize: 16, fontWeight: FontWeight.w400),),
                          const SizedBox(height: 50,),
                          Form(
                              key: _formKey,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              child: Column(children: [
                                const SizedBox(height: 20,),
                                TextFormField(
                                  decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(20),
                                        borderSide:  BorderSide(color: const Color(0xFFe3eaef) ),
                                      ),
                                      border: const OutlineInputBorder(),
                                      hintText: 'Email',
                                      hintStyle: const TextStyle(color: Color(0xFFe3eaef))
                                  ),
                                  validator: (value) {
                                    final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
                                    final regExp = RegExp(pattern);

                                    if (value!.isEmpty) {
                                      return 'Enter an email';
                                    } else if (!regExp.hasMatch(value)) {
                                      return 'Enter a valid email';
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  onSaved: (value) => setState(() => email = value!),
                                ),

                                const SizedBox(height: 20,),
                                Container(
                                    height: 55,
                                    width: 350,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
                                        color: const Color(0xFFFA8266)
                                    ),
                                    child: TextButton(
                                      onPressed: (){},
                                      child: Container(
                                        padding: const EdgeInsets.all(10),
                                        child: _isLoading
                                            ? const SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CircularProgressIndicator(),
                                        )
                                            : const Text('SEND NOW', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),),
                                      ),
                                    )
                                ),
                                const SizedBox(height: 70,),
                                Container(
                                    child: Center(child:
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const Text('Having Problem ?', style: TextStyle(color: Color(0xFF69798D), fontWeight: FontWeight.w400, fontSize: 14),),
                                        const SizedBox(width: 10,),
                                        GestureDetector(onTap: (){
                                          Navigator.pushNamed(context, registerPath);
                                        },
                                            child: const Text('Need Help', style: TextStyle(color: Color(0xFFFA8266), fontSize: 18, fontWeight: FontWeight.w600),)
                                        )
                                      ],)
                                    )
                                ),
                              ],))
                        ],))
                  )
                  )
              )
          )
      ),
    );
  }
}