import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:session4/routes/path.dart';
// import 'package:flutter_switch/flutter_switch.dart';

class RegisterScreen extends StatefulWidget {
  static String path = registerPath;
  const RegisterScreen({ Key? key }) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  bool isObscureText = true;
  bool status = false;
  var _isLoading = false;
  String phone = '';
  String password = '';
  String email ='';

  void _onSubmit() {
    setState(() => _isLoading = true);
    // Navigator.pushNamed(context, homePath);
    Future.delayed(
      const Duration(seconds: 2),

          () => setState(() => {_isLoading = false}),
    );

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xFFf6f9ff),
      body: SafeArea(
          child: SingleChildScrollView(
              child: Container(
                  child: Center(child: Padding(padding: EdgeInsets.symmetric(horizontal: 20), child:
                  Container(
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 50,),
                          const Text('SING UP', style: TextStyle(color: Color(0xFF404040), fontSize: 24, fontWeight: FontWeight.w600),),
                          const SizedBox(height: 20,),
                          const Text('Complete this step for best adjustment', style: TextStyle(color: Color(0xFF404040), fontSize: 16, fontWeight: FontWeight.w400),),
                          const SizedBox(height: 50,),
                          Form(
                              key: _formKey,
                              autovalidateMode: AutovalidateMode.onUserInteraction,
                              child: Column(children: [
                                Row(
                                  children: [
                                    const Text('Your Email', style: TextStyle(color: Color(0xFF69798D), fontSize: 16, fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                const SizedBox(height: 10,),
                                TextFormField(
                                  decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(20),
                                        borderSide:  BorderSide(color: const Color(0xFFe3eaef) ),
                                      ),
                                      border: const OutlineInputBorder(),
                                      hintText: 'Email',
                                      hintStyle: const TextStyle(color: Color(0xFFe3eaef))
                                  ),
                                  validator: (value) {
                                    final pattern = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)';
                                    final regExp = RegExp(pattern);

                                    if (value!.isEmpty) {
                                      return 'Enter an email';
                                    } else if (!regExp.hasMatch(value)) {
                                      return 'Enter a valid email';
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  onSaved: (value) => setState(() => email = value!),
                                ),
                                const SizedBox(height: 30,),
                                Row(
                                  children: [
                                    const Text('Password', style: TextStyle(color: Color(0xFF69798D), fontSize: 16, fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                const SizedBox(height: 10,),

                                TextFormField(
                                  obscureText: isObscureText,
                                  decoration: InputDecoration(
                                      enabledBorder: new OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(20),
                                        borderSide:  BorderSide(color: Color(0xFFe3eaef) ),
                                      ),
                                      border: const OutlineInputBorder(),
                                      hintText: 'Password',
                                      hintStyle: const TextStyle(color: Color(0xFFe3eaef))
                                  ),
                                  validator: (value) {
                                    if (value!.length < 7) {
                                      return 'Password must be at least 7 characters long';
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (value) => setState(() => password = value!),
                                  keyboardType: TextInputType.visiblePassword,
                                ),
                                const SizedBox(height: 30,),
                                Row(
                                  children: [
                                    const Text('Mobile', style: TextStyle(color: Color(0xFF69798D), fontSize: 16, fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                const SizedBox(height: 10,),
                                TextFormField(
                                  obscureText: false,
                                  decoration: InputDecoration(
                                      enabledBorder: new OutlineInputBorder(
                                        borderRadius: new BorderRadius.circular(20),
                                        borderSide:  BorderSide(color: Color(0xFFe3eaef) ),
                                      ),
                                      border: const OutlineInputBorder(),
                                      hintText: 'Phone',
                                      hintStyle: const TextStyle(color: Color(0xFFe3eaef))
                                  ),
                                  validator: (value) {
                                    if (value!.length < 4) {
                                      return 'Enter at least 4 characters';
                                    } else {
                                      return null;
                                    }
                                  },
                                  maxLength: 30,
                                  onSaved: (value) => setState(() => phone = value!),
                                ),
                                const SizedBox(height: 10,),
                                Container(
                                    child: Stack(
                                        children: [
                                          Padding(padding: EdgeInsets.only(right: 290),
                                            child:  FlutterSwitch(
                                              activeColor:  Color(0xFFFA8266),
                                              width: 50.0,
                                              height: 25.0,
                                              valueFontSize: 10.0,
                                              toggleSize: 10.0,
                                              value: status,
                                              borderRadius: 30.0,
                                              padding: 8.0,
                                              showOnOff: true,
                                              onToggle: (val) {
                                                setState(() {
                                                  status = val;
                                                });
                                              },
                                            ),
                                          ),
                                          Padding(padding: EdgeInsets.only(left: 80,top: 5),
                                              child: const Text("I accepted all tearms & conditions",style: TextStyle(
                                                  color: Color(0xFF69798D)
                                              ),)
                                          ),
                                        ]
                                    )
                                ),

                                const SizedBox(height: 30,),
                                Container(
                                    height: 55,
                                    width: 350,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
                                        color: const Color(0xFFFA8266)
                                    ),
                                    child: TextButton(
                                      onPressed: _isLoading ? null :  _onSubmit,
                                      child: Container(
                                        padding: const EdgeInsets.all(10),
                                        child: _isLoading
                                            ? const SizedBox(
                                          height: 25,
                                          width: 25,
                                          child: CircularProgressIndicator(),
                                        )
                                            : const Text('Sign Up', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w600),),
                                      ),
                                    )
                                ),
                                const SizedBox(height: 90,),
                                const SizedBox(height: 20,),
                                Container(
                                    child: Center(child:
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const Text('I already have a account ?', style: TextStyle(color: Color(0xFF69798D), fontWeight: FontWeight.w400, fontSize: 14),),
                                        const SizedBox(width: 10,),
                                        GestureDetector(onTap: (){
                                          Navigator.pushNamed(context, loginPath);
                                        },
                                            child: const Text('Sign IN', style: TextStyle(color: Color(0xFFFA8266), fontSize: 18, fontWeight: FontWeight.w600),)
                                        )
                                      ],)
                                    )
                                ),
                              ],))
                        ],))
                  )
                  )
              )
          )
      ),
    );
  }
}