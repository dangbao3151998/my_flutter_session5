import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:session4/components/body_my_order.dart';
import 'package:session4/routes/path.dart';

class MyOrderScreen extends StatefulWidget {
  static String path = myOrderPath;
  const MyOrderScreen({ Key? key }) : super(key: key);

  @override
  _MyOrderScreenState createState() => _MyOrderScreenState();
}

class _MyOrderScreenState extends State<MyOrderScreen> with TickerProviderStateMixin{

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  Widget _createTab(String text) {
    return Tab(
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: Container(
                    child: Center(child: Text(text)),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: Color(0xFFc3c3c3),
                          width: 0.6,
                        ),
                        bottom: BorderSide(
                            color: Colors.grey, width: 0.8
                        ),
                      ),
                    )
                )
            ),
          ]
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          // automaticallyImplyLeading: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            shadowColor: Colors.transparent,
            backgroundColor: Colors.transparent,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children:const [
                  Text('My Order',style: TextStyle(color: Colors.black)),],
              ),
            ],
          ),
          elevation: 0.1,
            bottom: TabBar(
              // change size underline tabBar
              indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(width: 2.0),
                  insets: EdgeInsets.symmetric(horizontal: 50.0),
              ),
              // nối liền đường thẳng bottom tabBar
              labelPadding: EdgeInsets.all(0.0),
              unselectedLabelColor: Colors.black,
              indicatorColor: Color(0xffF15C22),
              controller: _tabController,
              // indicatorWeight: 1,
              labelColor:  Color(0xFFFA8266),
               // indicatorColor: Color(0xFFFA8266),
              labelStyle: TextStyle(
                fontSize: 20,
              ),
              tabs: <Widget>[
                _createTab("Complete Order"),
                _createTab("Pending Order"),
              ],
            )
        ),

        body: const Body_My_Order(),
    );

  }
}
