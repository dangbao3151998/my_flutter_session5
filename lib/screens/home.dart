import 'dart:ui';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:session4/components/body_home.dart';
import 'package:flutter/material.dart';
import 'package:session4/routes/path.dart';
import "package:flutter_svg/flutter_svg.dart";

class HomeScreen extends StatefulWidget {
  static String path = homePath;
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xFFFFFFFF),
        leading: IconButton(
          icon: SvgPicture.asset("assets/icons/profile.svg",),
          tooltip: 'Show Snackbar',
          onPressed: () {
            ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('This is a snackbar')));
          },
        ),
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:const [
                Text('Deliver to',style: TextStyle(color: Colors.black)), Icon(Icons.arrow_drop_down)],
            ),
            const Text(
              'Parijat, Housing Estate', style: TextStyle(color: Color(0xFFFA8266)),
            )
          ],
        ),
        elevation: 0.1,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: Image.asset('assets/images/image1.png',height: 22,width: 50,),
          )
        ],
      ),
        body: const Body(),
        bottomNavigationBar: ConvexAppBar(
          activeColor: Color(0xFFFA8266),
          top: -40,
          curveSize: 120,
          height: 50,

          items: [
            TabItem(icon: SvgPicture.asset("assets/icons/home.svg",color: Colors.white,), title: 'Home',),
            TabItem(icon: SvgPicture.asset("assets/icons/search1.svg",color: Colors.white,), title: 'Search',),
            TabItem(icon: SvgPicture.asset("assets/icons/store.svg",color: Colors.white,), title: 'Search',),
            TabItem(icon: SvgPicture.asset("assets/icons/order.svg",color: Colors.white,),title: 'Order'),
            TabItem(icon: SvgPicture.asset("assets/icons/persion.svg",color: Colors.white,), title: 'Account'),
          ],
          initialActiveIndex: 2,//optional, default as 0
          // onTap: (int i) => print('click index=$i'),
          onTap: (int i) => Navigator.pushNamed(context, myOrderPath),
        )
    );

  }
}
