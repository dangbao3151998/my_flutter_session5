const String splashPath = '/';
const String forgotPasswordPath = '/forgot-password';
const String flaskLogin = '/flask-login';
const String loginPath = '/login';
const String registerPath = '/register';
const String homePath = '/home';
const String myOrderPath = '/my_order';