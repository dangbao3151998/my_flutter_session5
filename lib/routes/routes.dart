import 'package:flutter/material.dart';
// import 'package:session2/home/home.dart';
// import 'package:session2/screens/home.dart';
import 'package:session4/screens/flask_login.dart';
import 'package:session4/screens/home.dart';
import 'package:session4/screens/loginn.dart';
import 'package:session4/screens/my_order.dart';
import 'package:session4/screens/sing_up.dart';
import 'package:session4/screens/forgot_password.dart';
import 'package:session4/screens/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.path: (context) => const SplashScreen(),
  RegisterScreen.path: (context) => const RegisterScreen(),
  HomeScreen.path: (context) => const HomeScreen(),
  FlaskLogin.path: (context) => const FlaskLogin(),
  LoginScreen.path: (context) => const LoginScreen(),
  ForgotPasswordScreen.path: (context) => const ForgotPasswordScreen(),
  MyOrderScreen.path: (context) => const MyOrderScreen(),
};