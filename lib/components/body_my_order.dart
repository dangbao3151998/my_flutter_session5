import 'package:flutter/material.dart';
import 'my_order_cake.dart';

class Body_My_Order extends StatelessWidget {
  const Body_My_Order({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // It will provie us total height  and width of our screen
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(

      child: Column(
        children: <Widget>[
          const MyOrderCake(),
        ],
      ),
    );
  }
}