import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:session4/routes/path.dart';

class Splash extends StatefulWidget {

  @override
  _SplashState createState() => _SplashState();
}


class _SplashState extends State<Splash> {
  int currentPage = 0;
  List<Map<String, String >> splashData = [
    {
      "text":"GPS Tracking",
      "image":"assets/images/image2.png"
    },
    {
      "text":"GPS Tracking",
      "image":"assets/images/image3.png"
    },
  ];
  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: SizedBox(
        child:Column(
          children: <Widget>[
            Expanded(
                flex: 3,
                child: PageView.builder(
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                    itemCount: splashData.length,
                    itemBuilder: (context,index) => SplashContent(
                  image: splashData[index]["image"],
                  text: splashData[index]["text"],
                ))
            ),
            Expanded(
              flex: 1,
              child: Column(
                // color: Colors.red,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      splashData.length,
                          (index) => buildDot(index: index),
                    ),
                  ),
                  Spacer(flex: 2,),
                  SizedBox(
                    width: 370,
                      height: 50,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                          color: Colors.red,
                          onPressed: (){ Navigator.pushNamed(context, flaskLogin);},
                          child: Text("Continue", style: TextStyle(color: Colors.white),))

                  ),
                  SizedBox(height: 20,)
                  // Spacer(),
                ],
              ),
            ),
          ],
        )
    )
    );
  }
  Container buildDot({required int index}) {

    return Container(
      // duration: kAnimationDuration,
        margin: EdgeInsets.only(right: 5),
        height: 7,
        width:  currentPage == index ? 20 : 6,
        decoration: BoxDecoration(
        color:  currentPage == index ? Colors.red : Color(0xFFD8D8D8),
         borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}



class SplashContent extends StatelessWidget {
  const SplashContent({
    Key? key,
    this.text,
    this.image,
  }) : super(key: key);
  final String? text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const Spacer(),
        Container(
        width: double.infinity,
        child: Stack(
        children: <Widget>[

          GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, flaskLogin);
              },
              child:
              const Padding(padding: EdgeInsets.only(top: 3, left: 350), child:
                  Text("skip",style: TextStyle(color: Color(0xFFFA8266)),)
              )
          ),
          GestureDetector(
              onTap: (){
                // widget.onTapSkip();
                Navigator.pushNamed(context, flaskLogin);
              },
              child:
              const Padding(padding: EdgeInsets.only(top: 0, left: 370), child:
                Icon(Icons.arrow_forward_ios_rounded, color: Color(0xFFFA8266),),
              )
          ),
          ]
        )
        ),
        Spacer(flex: 2),
        Image.asset(
          image!,
          height: 350,
          width: 250,
        ),
        Spacer(flex: 2),
        Text(
          text!,
          style: TextStyle(
            fontWeight: FontWeight.bold,fontSize: 20,
            color: Color(0xFF353536)
          ),
        ),
        Spacer(flex: 1),
        Text(
          "Loved tje class! Such beautiful land and \n collective impact infrastructure social \n entrepreneur.",
          style: TextStyle(
              color: Color(0xFF69798D)
          ),
          textAlign: TextAlign.center,
        ),
        Spacer(flex: 2),
      ],
    );
  }
}
