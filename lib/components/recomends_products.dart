import 'package:flutter/material.dart';
import '../../../constants.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: <Widget>[
          ProductsCard(
            image: "assets/images/images_cake1.png",
            title: "Panshin in",
            image1: "assets/images/images_cake3.png",
            country: "assets/images/images_cake2.png",
            price: 440,
            press: () {
            },
          ),
          ProductsCard(
            image: "assets/images/images_cake1.png",
            title: "Panshin in",
            image1: "assets/images/images_cake3.png",
            country: "assets/images/images_cake2.png",
            price: 440,
            press: () {
            },
          ),
          ProductsCard(
            image: "assets/images/images_cake1.png",
            title: "Panshin in",
            image1: "assets/images/images_cake3.png",
            country: "assets/images/images_cake2.png",
            price: 440,
            press: () {},
          ),
        ],
      ),
    );
  }
}

class ProductsCard extends StatelessWidget {
  const ProductsCard({
    Key? key,
    required this.image,
    required this.image1,
    required this.title,
    required this.country,
    required this.price,
    required this.press,
  }) : super(key: key);

  final String image, title, country,image1;
  final int price;
  final Function press;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
        onTap: () {
          // Navigator.pushNamed(context, detailPath);
        },
        child: Container(
          margin: EdgeInsets.only(
            left: kDefaultPadding,
            top: kDefaultPadding / 2,
            bottom: kDefaultPadding * 1,
          ),
          width: size.width * 0.4,
          child: Stack(
            children: <Widget>[
              // Image.asset(image,width: 500,),
              Container(
                padding: EdgeInsets.all(100.0 / 1),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.grey),
                ),
              ),
              Positioned(
                child: Container(
                    margin: EdgeInsets.only(top: 0, left: 0),
                    height: 120,
                    width: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                        ),
                        image: DecorationImage(
                          image: new AssetImage(image),
                          fit: BoxFit.fill,
                        )
                    )
                ),
              ),
              Positioned(
                child: Container(
                    margin: EdgeInsets.only(top: 0, left: 100),
                    height: 60,
                    width: 65,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                        ),
                        image: DecorationImage(
                          image: new AssetImage(country),
                          fit: BoxFit.fill,
                        )
                    )
                ),
              ),
              Positioned(
                child: Container(
                    margin: EdgeInsets.only(top: 60, left: 100),
                    height: 60,
                    width: 65,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(0),
                        ),
                        // borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image: new AssetImage(image1),
                          fit: BoxFit.fill,
                        )
                    )
                ),
              ),
              Positioned(child: Container(
                padding: EdgeInsets.only(left: 15,top: 145),
                child: Column(
                  children: <Widget>[
                    Text(title,style: Theme.of(context).textTheme.button,),
                  ],
                ),
              ),),
              Positioned(
                child: Container(
                  padding: EdgeInsets.only(left: 15,top: 168),
                  child: Text("4.9 recipes",style: TextStyle(fontWeight: FontWeight.w300),),
                ),
              ),
            ],
          ),
        )
    );
  }
}