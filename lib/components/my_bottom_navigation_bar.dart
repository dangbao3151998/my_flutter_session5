import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBottomNavigation extends StatelessWidget {
  const MyBottomNavigation({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {

    return BottomNavigationBar(
      selectedItemColor: Color(0xFFFA8266),
      unselectedItemColor: Colors.grey.withOpacity(0.5),
      showUnselectedLabels: false,
      showSelectedLabels: false,
      // currentIndex: snapshot.data.index,
      // onTap: _bottomNavBarBloc.pickItem,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.data_usage),
          label: 'discount',

        ),

        BottomNavigationBarItem(
          label: 'discount',
          icon: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.orange,
            ),
            child: FlutterLogo(
              size: 38.0,
            ),
          ),
        ),
        // BottomNavigationBarItem(
        //   icon: Icon(Icons.person),
        //   label: 'person',
        // ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'person',
        ),
      ],
    );
  }
}