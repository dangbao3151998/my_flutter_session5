import 'package:flutter/material.dart';
import 'recomends_products.dart';
import 'header.dart';
import 'product_card.dart';
import 'title_with_more_bbtn.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // It will provie us total height  and width of our screen
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          HeaderWithSearchBox(size: size),
          TitleWithMoreBtn(title: "Best for you", press: () {}),
          const RecomendsProducts(),
          TitleWithMoreBtn(title: "Best for you", press: () {}),
          const ProductCard(),
        ],
      ),
    );
  }
}