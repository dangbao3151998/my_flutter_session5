import 'package:flutter/material.dart';
import '../../../constants.dart';

class RecomendsProducts extends StatelessWidget {
  const RecomendsProducts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // It  will provide us total height and width of our screen
    // Size size = MediaQuery.of(context).size;
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.only(top: 0.10, right: 0.0, left: 0.0),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 3,
        reverse: true,
        itemBuilder: (context, index) => GestureDetector(
          // onTap: () {Navigator.pushNamed(context, detailPath);},
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: kDefaultPadding,
              vertical: kDefaultPadding / 2,
            ),
            // color: Colors.red,
            height: 70,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 80,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      border: Border.all(color: Colors.grey)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 5, left: 5),
                  height: 60,
                    width: 70,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image: new AssetImage("assets/images/images_station1.png"),
                          fit: BoxFit.fill,
                        )
                    )
                  // child: Image.asset("assets/images/images_station1.png"),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 100, top: 15),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Lorem Chair\n",
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 100, top: 45),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Jail rood. Zinda Bazar, Sylhet",
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 330, top: 25),
                    child: Icon(
                      Icons.star,
                      color: Colors.yellow,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
