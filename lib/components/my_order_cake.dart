import 'package:flutter/material.dart';
import '../../../constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyOrderCake extends StatelessWidget {
  const MyOrderCake({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // It  will provide us total height and width of our screen
    // Size size = MediaQuery.of(context).size;
    return Container(
      child: ListView.builder(
        padding: EdgeInsets.only(top: 0.10, right: 0.0, left: 0.0),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: 3,
        reverse: true,
        itemBuilder: (context, index) => GestureDetector(
          // onTap: () {Navigator.pushNamed(context, detailPath);},
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: kDefaultPadding,
              vertical: kDefaultPadding / 2,
            ),
            // color: Colors.red,
            height: 110,
            child: Stack(
              children: <Widget>[
                Container(
                  // height: 500,
                  // width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      border: Border.all(color: Colors.white)),
                ),
                Container(
                    margin: EdgeInsets.only(top: 5, left: 5),
                    height: 100,
                    width: 90,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          image: new AssetImage("assets/images/images_station1.png"),
                          fit: BoxFit.fill,
                        )
                    )
                  // child: Image.asset("assets/images/images_station1.png"),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 110, top: 15),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "4 Item from KFC\n",
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 110, top: 45),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Pizza, Alo Bortha, Thethul acar,\nChiken tiriaky",style: TextStyle( fontWeight: FontWeight.w300)
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 340, top: 20),
                    child: Column(
                      children: <Widget>[
                        Text(
                            "\$59",style: TextStyle( fontWeight: FontWeight.bold)
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 110, top: 80),
                      child: Column(
                        children: <Widget>[
                          SvgPicture.asset("assets/icons/fire.svg",)
                        ],
                      )
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 140, top: 85),
                      child: Column(
                        children: <Widget>[
                          Text(
                              "Spicy",style: TextStyle( color: Colors.black,fontWeight: FontWeight.w500)
                          ),
                        ],
                      )
                  ),
                ),
                Positioned(
                  child: Container(
                    padding: EdgeInsets.only(left: 280, top: 80),
                    child: Column(
                      children: <Widget>[
                        Text(
                            "ORder Again",style: TextStyle( color: Color(0xFFFA8266),fontWeight: FontWeight.w500)
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
